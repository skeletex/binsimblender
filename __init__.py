import bpy
import importlib
import configparser
import os
from . import ui
from . import mat_lib
from . import operators

bl_info = {
    "name": "BinSimBlender",
    "author": "Skeletex Research s.r.o.",
    "description": "BinSim integration plugin.",
    "blender": (3, 2, 1),
    "version": (2022, 8, 31),
    "location": "3D Viewport > Properties > BinSim",
    "category": "Render",
    "wiki_url": "https://www.skeletex.xyz/",
    "tracker_url": "https://www.skeletex.xyz/"
}

class BsData(bpy.types.PropertyGroup):
    p = bpy.props

    part_template: p.StringProperty(name="Part template", default="")
    bin_template: p.StringProperty(name="Bin template", default="")
    pattern_dir: p.StringProperty(name="Pattern directory", subtype="DIR_PATH")
    pattern_image_smooth: p.BoolProperty(name="Projector pattern smooth", default=True)
    pattern_include_none: p.BoolProperty(name="Include render without pattern", default=True)
    scene_map_file: p.StringProperty(name="Scene paths file", subtype="FILE_PATH")

    # ----- Config -----

    # debug
    verbose: p.BoolProperty(name="Verbose debug outputs", default=True)
    save_blend_file: p.BoolProperty(name="Save blend file before render", default=True)
    # general
    scan_scale_unit: p.FloatProperty(name="Unit scale of imported scans", default=1.0)
    render_sample_count: p.IntProperty(name="Render sample count", default=10)
    # library materials
    use_material_library: p.BoolProperty(name="Enable linking of library materials", default=False)
    material_library_path: p.StringProperty(name="Material library file", subtype="FILE_PATH")
    part_material_name: p.StringProperty(name="Name of library material to use for part")
    bin_material_name: p.StringProperty(name="Name of library material to use for bin")
    texture_coordinate: p.StringProperty(name="Texture coordinate node mapping method", default="Generated")
    # supported coordinates: Generated, Normal, UV, Object, Camera, Window, Reflection
    # lighting
    hdri_enable: p.BoolProperty(name="Enable HDRI lighting", default=False)
    hdri_path: p.StringProperty(name="Path of HDRI to be used", subtype="FILE_PATH")
    environment_brightness: p.FloatProperty(name="HDRI brightness / ambient brightness if hdri off", default=1.0)
    projector_brightness: p.FloatProperty(name="Projector energy", default=1000.0)
    # camera
    clip_start: p.FloatProperty(name="Camera near clipping plane", default=100.0)
    clip_end: p.FloatProperty(name="Camera far clipping plane", default=100000.0)
    # render passes
    output_rgb: p.BoolProperty(name="Render and export color pass", default=True)
    output_depth: p.BoolProperty(name="Render and export depth map", default=True)
    output_normal: p.BoolProperty(name="Render and export normal map", default=True)

    @staticmethod
    def config(config_path):
        config = configparser.ConfigParser()
        config.read(os.path.abspath(config_path))

        if len(config.sections()) == 0:
            raise RuntimeError("BinSimBlender: Blender config file could not be parsed.")

        val = bpy.utils.units.to_value
        scale_length = val("METRIC", "LENGTH",	config['general']['scan_scale_unit'])

        BsData.verbose = 						config['debug'].getboolean('verbose')
        BsData.save_blend_file = 				config['debug'].getboolean('save_blend_file')
        BsData.scan_scale_unit =				scale_length
        BsData.render_sample_count =			config['general'].getint('render_sample_count')
        BsData.use_material_library =			config['material'].getboolean('use_material_library')
        BsData.material_library_path =			config['material']['material_library_path']
        BsData.part_material_name = 			config['material']['part_material_name']
        BsData.bin_material_name =				config['material']['bin_material_name']
        BsData.texture_coordinate = 			config['material']['texture_coordinate']
        BsData.hdri_enable =					config['lighting'].getboolean('hdri_enable')
        BsData.hdri_path =                      config['lighting']['hdri_path']
        BsData.environment_brightness = 		config['lighting'].getfloat('environment_brightness')
        BsData.projector_brightness = val("METRIC", "POWER",
                                          config['lighting']['projector_brightness']) / scale_length ** 2
        BsData.clip_start = val("METRIC", "LENGTH",
                                config['camera']['clip_start']) / scale_length
        BsData.clip_end = val("METRIC", "LENGTH",
                              config['camera']['clip_end']) / scale_length
        BsData.output_rgb =						config['render passes'].getboolean('output_rgb')
        BsData.output_depth =					config['render passes'].getboolean('output_depth')
        BsData.output_normal =					config['render passes'].getboolean('output_normal')


    @staticmethod
    def apply():
        scene = bpy.context.scene
        layer = bpy.context.view_layer

        bpy.app.debug_wm = BsData.verbose
        scene.unit_settings.scale_length = BsData.scan_scale_unit
        scene.eevee.taa_render_samples = BsData.render_sample_count

        if BsData.use_material_library:
            parts = mat_lib.get_part_objects()
            bin = scene.objects['BinSim.Bin']

            mat_lib.link_materials(os.path.abspath(BsData.material_library_path),
                                   [BsData.bin_material_name, BsData.part_material_name])

            #mat_lib.link_materials_from_directory(os.path.abspath(BsData.material_library_path),
            #                                     [BsData.bin_material_name, BsData.part_material_name])
            mat_lib.apply_mat_to_objects(BsData.bin_material_name, [bin])
            mat_lib.apply_mat_to_objects(BsData.part_material_name, parts)
            mat_lib.change_texture_coordinate(BsData.bin_material_name, BsData.texture_coordinate)
            mat_lib.change_texture_coordinate(BsData.part_material_name, BsData.texture_coordinate)

        if BsData.hdri_enable:
            # todo change brightness of hdri
            mat_lib.import_hdri(os.path.abspath(BsData.hdri_path))
        else:
            mat_lib.set_ambient_light(BsData.environment_brightness)

        layer.use_pass_combined = BsData.output_rgb
        layer.use_pass_mist = BsData.output_depth
        layer.use_pass_normal = BsData.output_normal


def register():
    importlib.reload(ui)
    importlib.reload(operators)

    bpy.utils.register_class(BsData)
    bpy.types.Scene.bs_data = bpy.props.PointerProperty(type=BsData)

    BsData.config("./config_blender.txt")

    operators.register()
    ui.register()


def unregister():
    bpy.utils.unregister_class(BsData)

    operators.unregister()
    ui.unregister()
