import bpy
import bpy_extras
import bmesh
import math
import mathutils
import os
import sys
from random import randrange
from bpy.types import Operator
from . import mat_lib


def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [ alist[i*length // wanted_parts: (i+1)*length // wanted_parts]
             for i in range(wanted_parts) ]


def remove_if_exists(name, data_collection):
    object = data_collection.get(name)
    if object != None:
        data_collection.remove(object)


def get_directory(filename):
    last_slash = filename.rfind("/")
    return filename[:last_slash+1]


def read_floats(file):
    line = file.readline()
    return [float(s) for s in line.split()]


def import_matrices(filename):
    matrices = []
    file_in = open(filename, 'r')
    for line in file_in.read().split('\n'):
        values = [float(s) for s in line.split()]
        if len(values) == 16:
            rot_correction = mathutils.Matrix.to_4x4(
                bpy_extras.io_utils.axis_conversion(from_up='-Z', to_up='Z'))
            mat = mathutils.Matrix(split_list(values, 4)) @ rot_correction
            matrices.append(mat.transposed())
    return matrices


def import_mesh_to_col(filename, collection_name):
    bpy.ops.import_mesh.stl(filepath=filename, axis_forward='Y', axis_up='-Z')
    object = bpy.context.selected_objects[0]
    bpy.context.scene.collection.objects.unlink(object)
    bpy.data.collections[collection_name].objects.link(object)
    return object


def reset_collection(collection_name):
    collection_id_to_remove = bpy.data.collections.find(collection_name)
    if collection_id_to_remove != -1:
        bpy.ops.object.select_all(action='DESELECT')
        for col_object in bpy.data.collections[collection_id_to_remove].objects:
            col_object.select_set(True)
        bpy.ops.object.delete()
        bpy.data.collections.remove(bpy.data.collections[collection_id_to_remove])
    new_collection = bpy.data.collections.new(collection_name)
    bpy.context.scene.collection.children.link(new_collection)
    return new_collection


def import_template(dataset_dir):
    template_collection = reset_collection("Templates")

    part_object = import_mesh_to_col(dataset_dir+"part.stl", template_collection_name)
    part_object.name = "PartTemplate"

    bin_object = import_mesh_to_col(dataset_dir+"bin.stl", template_collection_name)
    bin_object.name = "BinTemplate"


screen_material_name = "BinSim.Mt.Projector.Screen"

def create_projector_occluder_materials(mesh_to_apply_on):

    occluder_material_name = "BinSim.Mt.Projector.Occluder"
    remove_if_exists(occluder_material_name, bpy.data.materials)
    mt_occluder = bpy.data.materials.new(occluder_material_name)
    mesh_to_apply_on.materials.append(mt_occluder)

    remove_if_exists(screen_material_name, bpy.data.materials)
    mt_pattern = bpy.data.materials.new(screen_material_name)
    mt_pattern.use_nodes = True
    mt_pattern.blend_method = 'BLEND'
    mt_pattern.shadow_method = 'CLIP'

    nodes = mt_pattern.node_tree.nodes
    links = mt_pattern.node_tree.links
    nodes.clear()

    final = nodes.new('ShaderNodeOutputMaterial')
    final.location = (750, 0)

    principled = nodes.new('ShaderNodeBsdfPrincipled')
    principled.location = (450, 200)
    principled.inputs['Alpha'].default_value = 0.0
    links.new(principled.outputs['BSDF'], final.inputs['Surface'])

    binsim_data = bpy.context.scene.bs_data
    pattern_tex = nodes.new('ShaderNodeTexImage')
    pattern_tex.location = (-250, 50)
    # pattern_tex.image = binsim_data.pattern_image
    pattern_tex.interpolation = "Linear" if binsim_data.pattern_image_smooth else "Closest"
    bpy.context.scene.bs_data.pattern_tex_image = pattern_tex

    rgb_to_bw = nodes.new('ShaderNodeRGBToBW')
    rgb_to_bw.location = (50, 0)
    links.new(pattern_tex.outputs['Color'], rgb_to_bw.inputs['Color'])

    invert = nodes.new('ShaderNodeMath')
    invert.location = (250, 0)
    invert.operation = 'SUBTRACT'
    invert.inputs[0].default_value = 1
    links.new(rgb_to_bw.outputs['Val'], invert.inputs[1])

    links.new(invert.outputs['Value'], principled.inputs['Alpha'])

    mesh_to_apply_on.materials.append(mt_pattern)


def set_pattern_image(image):
    mt = bpy.data.materials.get(screen_material_name)
    tex_node = mt.node_tree.nodes['Image Texture']
    math_node = mt.node_tree.nodes['Math']

    tex_node.image = image
    if image == None:
        math_node.inputs[0].default_value = 0
    else:
        math_node.inputs[0].default_value = 1


def create_projector_occluder(fovx_deg, fovy_deg, collection):
    scale = 100.0
    xd = scale * math.tan(0.5 * math.radians(fovx_deg))
    yd = scale * math.tan(0.5 * math.radians(fovy_deg))

    verts = [
        (xd, yd, -scale),
        (xd, -yd, -scale),
        (-xd, -yd, -scale),
        (-xd, yd, -scale),
        (0, 0, scale)]

    faces = [
        (0, 1, 2, 3),
        (0, 4, 1),
        (1, 4, 2),
        (2, 4, 3),
        (4, 0, 3)]

    projector_occluder_name = "BinSim.Projector.Occluder"
    remove_if_exists(projector_occluder_name, bpy.data.meshes)
    mesh = bpy.data.meshes.new(projector_occluder_name)
    mesh.from_pydata(verts, [], faces)
    mesh.uv_layers.new(name="Projector Screen UVS")
    uvs = mesh.uv_layers.active.data
    uvs[0].uv = (0,0)
    uvs[1].uv = (0,1)
    uvs[2].uv = (1,1)
    uvs[3].uv = (1,0)

    obj = bpy.data.objects.new(projector_occluder_name, mesh)
    collection.objects.link(obj)

    create_projector_occluder_materials(mesh)

    # set front plane of occluder to a different material
    bm = bmesh.new()
    bm.from_mesh(obj.data)
    bm.faces.ensure_lookup_table()
    bm.faces[0].material_index = 1
    bm.to_mesh(obj.data)
    bm.free()

    return obj


def import_scanner(scene_path, collection):
    bs_data = bpy.context.scene.bs_data
    scale_length = bpy.context.scene.unit_settings.scale_length

    scanner_camera_name = "BinSim.Camera"
    remove_if_exists(scanner_camera_name, bpy.data.cameras)
    scanner_camera = bpy.data.cameras.new(scanner_camera_name)

    scanner_camera_object = bpy.data.objects.new(scanner_camera_name, scanner_camera)
    scanner_camera_object.scale = (100, 100, 100)
    collection.objects.link(scanner_camera_object)

    scanner_projector_name = "BinSim.Projector"
    remove_if_exists(scanner_projector_name, bpy.data.lights)
    projector_light = bpy.data.lights.new(name=scanner_projector_name, type='POINT')

    projector_light.energy = bs_data.projector_brightness
    projector_light.shadow_soft_size = 0.0
    projector = bpy.data.objects.new(name=scanner_projector_name, object_data=projector_light)
    collection.objects.link(projector)
    projector.scale = (100, 100, 100)

    render = bpy.context.scene.render

    f = open(scene_path + "_scanner_params.txt")

    [width, height] = read_floats(f)
    render.resolution_x = int(width)
    render.resolution_y = int(height)
    render.resolution_percentage = 100

    [fx, fy, cx, cy] = read_floats(f)
    camera_fovx = 2.0 * math.atan(cx / fx)
    scanner_camera.angle = camera_fovx
    scanner_camera.clip_start = bs_data.clip_start
    scanner_camera.clip_end = bs_data.clip_end

    [baseline, cam_angle, proj_angle] = read_floats(f)
    z_displacement = baseline * math.sin(math.radians(cam_angle))
    x_displacement = baseline * math.cos(math.radians(cam_angle))
    projector.location = (-x_displacement, 0, z_displacement)
    projector.rotation_euler = (0, math.radians(- cam_angle - proj_angle), 0)

    [proj_fovx, proj_fovy] = read_floats(f)
    occluder = create_projector_occluder(proj_fovx, proj_fovy, collection)
    occluder.location = projector.location
    occluder.rotation_euler = projector.rotation_euler

    f.close()


def validate_binsim_data(binsim_data, logger):
    success = True
    if not binsim_data.bin_template:
        logger.report({'ERROR'}, "Bin template object was not specified.")
        success = False
    elif bpy.data.objects.get(binsim_data.bin_template) == None:
        logger.report({'ERROR'}, "Object with name " + binsim_data.bin_template + " can not be used as bin template. The object does not exist.")
        success = False
    if not binsim_data.part_template:
        logger.report({'ERROR'}, "Part template object was not specified.")
        success = False
    elif bpy.data.objects.get(binsim_data.part_template) == None:
        logger.report({'ERROR'}, "Object with name " + binsim_data.bin_template + " can not be used as part template. The object does not exist.")
        success = False
    return success


def import_scene(scene_path, binsim_data):
    bin_transform = import_matrices(scene_path + "_bin_transform.txt")[0]
    part_transforms = import_matrices(scene_path + "_part_transforms.txt")

    scene_collection = reset_collection("BinSim.Scene")

    bin = bpy.data.objects[binsim_data.bin_template].copy()
    bin.name = "BinSim.Bin"
    bin.matrix_world = bin_transform
    scene_collection.objects.link(bin)
    for transform in part_transforms:
        part = bpy.data.objects[binsim_data.part_template].copy()
        part.name = "BinSim.Part.000"
        part.matrix_world = transform
        scene_collection.objects.link(part)

    import_scanner(scene_path, scene_collection)
    bpy.context.scene.camera = bpy.data.objects.get("BinSim.Camera")

    object_count = len(bpy.context.scene.objects)
    bpy.context.scene.objects.foreach_set("hide_render", [True]*object_count)
    bin_sim_collection = bpy.data.collections.get("BinSim.Scene")
    for o in bin_sim_collection.objects:
        o.hide_render = False

    return True


def load_pattern_images():
    pattern_dir = bpy.path.abspath(bpy.context.scene.bs_data.pattern_dir)
    pattern_dir = pattern_dir.replace("\\", "/")
    if pattern_dir == '':
        return []
    pattern_filenames = os.listdir(pattern_dir)
    patterns = []
    for pattern_filename in pattern_filenames :
        print ("loading pattern file " + pattern_filename)
        patterns.append(bpy.data.images.load(pattern_dir + pattern_filename, check_existing=False))
    return patterns


class BINSIM_OT_preview(bpy.types.Operator):
    """Import scene of bin filled with parts."""
    bl_idname = 'binsim.preview'
    bl_label = 'Import scene of bin filled with parts.'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        binsim_data = bpy.context.scene.bs_data
        binsim_data.apply()
        mat_lib.render_setup()
        if not validate_binsim_data(binsim_data, self):
            return {'CANCELLED'}

        map_file = bpy.path.abspath(binsim_data.scene_map_file)
        map_file = map_file.replace("\\", "/")
        map_dir = get_directory(map_file)
        with open(map_file, 'r') as file_in:
            scene_path = map_dir + file_in.readline().rstrip()
            import_scene(scene_path, binsim_data)

            patterns = load_pattern_images()
            if len(patterns) == 0:
                set_pattern_image(None)
            else:
                i = randrange(len(patterns))
                set_pattern_image(patterns[i])

        return {'FINISHED'}


class BINSIM_OT_render_dataset(bpy.types.Operator):
    """Import every scene of dataset and render with specified patterns."""
    bl_idname = 'binsim.render_dataset'
    bl_label = 'Import every scene of dataset and render with specified patterns.'

    def render_pattern_image(self, scene_path, pattern_image, id):
        set_pattern_image(pattern_image)
        image_file = scene_path + "_colors_blender_" + id + ".exr"
        bpy.context.scene.render.filepath = image_file
        if bpy.context.scene.bs_data.save_blend_file:
            bpy.ops.wm.save_as_mainfile(filepath=scene_path + "_colors_blender_" + id + ".blend")
        bpy.ops.render.render(write_still=True)

    def render_pattern_image_compositor(self, scene_path, pattern_image, id):
        set_pattern_image(pattern_image)
        image_file = scene_path + "_colors_blender_" + id + ".exr"
        bpy.context.scene.render.filepath = image_file
        mat_lib.compositor_setup()
        if bpy.context.scene.bs_data.save_blend_file:
            bpy.ops.wm.save_as_mainfile(filepath=scene_path + "_colors_blender_" + id + ".blend")
        bpy.ops.render.render(write_still=False) # write_still replaced with file output node

    def execute(self, context):
        binsim_data = bpy.context.scene.bs_data
        binsim_data.apply()
        mat_lib.render_setup()
        if not validate_binsim_data(binsim_data, self):
            return {'CANCELLED'}

        patterns = load_pattern_images()

        map_file = bpy.path.abspath(binsim_data.scene_map_file)
        map_file = map_file.replace("\\", "/")
        map_dir = get_directory(map_file)
        with open(map_file, 'r') as file_in:
            for line in file_in:
                scene_path = map_dir + line.rstrip()
                import_scene(scene_path, binsim_data)
                for i in range(0,len(patterns)):
                    self.render_pattern_image(scene_path, patterns[i], "p" + str(i))
                if binsim_data.pattern_include_none:
                    self.render_pattern_image(scene_path, None, "full")

        return {'FINISHED'}


def register():
    bpy.utils.register_class(BINSIM_OT_preview)
    bpy.utils.register_class(BINSIM_OT_render_dataset)


def unregister():
    bpy.utils.unregister_class(BINSIM_OT_preview)
    bpy.utils.unregister_class(BINSIM_OT_render_dataset)