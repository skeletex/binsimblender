import bpy
import os


def get_filename(filepath):
    return filepath.rsplit("\\")[-1].rsplit("/")[-1]


def scan_blend_files(directory):
    blend_paths = []
    walked = os.walk(directory)

    for dirpath, dirnames, filenames in walked:
        blend_files_here = list(filter(lambda filename: filename.endswith('.blend'), filenames))
        for filename in blend_files_here:
            blend_path = os.path.join(dirpath, filename)
            blend_paths.append(blend_path)

    return blend_paths


def scan_materials(directory):
    blend_paths = scan_blend_files(directory)
    materials = {}

    for filepath in blend_paths:
        with bpy.data.libraries.load(filepath) as (data_from, data_to):
            materials[filepath] = data_from.materials

    return materials


def link_materials_from_directory(directory, materials):
    blend_paths = scan_blend_files(directory)

    for filepath in blend_paths:
        with bpy.data.libraries.load(filepath) as (data_from, data_to):
            mats_in_file = set(materials).intersection(set(data_from.materials))

            if mats_in_file:
                data_to.materials = list(mats_in_file)


def link_material_lib(filepath):
    with bpy.data.libraries.load(filepath) as (data_from, data_to):
        data_to.materials = data_from.materials


def link_materials(filepath, mat_names):
    with bpy.data.libraries.load(filepath) as (data_from, data_to):
        data_to.materials = mat_names


def set_ambient_light(brightness):
    world = bpy.context.scene.world

    world.use_nodes = False
    world.color = (brightness, brightness, brightness)


def import_hdri(filepath):
    bpy.context.scene.world.use_nodes = True
    tree = bpy.context.scene.world.node_tree
    tree.nodes.clear()

    node_coord = tree.nodes.new("ShaderNodeTexCoord")
    node_mapping = tree.nodes.new("ShaderNodeMapping")
    node_texture = tree.nodes.new("ShaderNodeTexImage")
    node_background = tree.nodes.new("ShaderNodeBackground")
    node_output = tree.nodes.new("ShaderNodeOutputWorld")

    bpy.ops.image.open(filepath=filepath)
    image_name = get_filename(filepath)
    image = bpy.data.images[image_name]
    node_texture.image = image

    tree.links.new(node_coord.outputs["Generated"], node_mapping.inputs["Vector"])
    tree.links.new(node_mapping.outputs["Vector"], node_texture.inputs["Vector"])
    tree.links.new(node_texture.outputs["Color"], node_background.inputs["Color"])
    tree.links.new(node_background.outputs["Background"], node_output.inputs["Surface"])

    for i, node in enumerate((node_coord, node_mapping, node_texture, node_background, node_output)):
        node.location = (150 * i, 0)


def change_texture_coordinate(mat_name, output_name):
    tree = bpy.data.materials[mat_name].node_tree

    links = tree.links.values()
    node_coordinate = tree.nodes.get('Texture Coordinate')

    if node_coordinate is None:
        return

    coord_links = filter(lambda link: link.from_node.name == 'Texture Coordinate', links)

    for link in coord_links:
        tree.links.new(node_coordinate.outputs[output_name], link.to_socket)


def get_linked_materials():
    return list(filter(lambda mat: mat.library is not None, bpy.data.materials.values()))


def get_linked_materials(lib):
    return list(filter(lambda mat: mat.library == lib, bpy.data.materials.values()))


def get_part_objects():
    return list(filter(lambda obj:obj.name.startswith('BinSim.Part.'), bpy.data.objects))


def apply_mat_to_objects(mat_name, objects):
    for obj in objects:
        obj.material_slots.data.active_material = bpy.data.materials.get(mat_name)


def render_setup():
    scene = bpy.context.scene
    layer = bpy.context.view_layer

    scene.use_nodes = False
    scene.render.use_compositing = False

    scene.render.use_file_extension = False

    scene.world.mist_settings.start = scene.camera.data.clip_start
    scene.world.mist_settings.depth = scene.camera.data.clip_end
    scene.world.mist_settings.falloff = 'LINEAR'

    scene.render.image_settings.file_format = 'OPEN_EXR_MULTILAYER'
    scene.render.image_settings.color_mode = 'RGBA'
    scene.render.image_settings.color_depth = '16'
    scene.render.image_settings.use_preview = True         # todo disable
    scene.render.image_settings.color_management = 'FOLLOW_SCENE'

    layer.use_pass_combined = True
    layer.use_pass_environment = False
    # Z pass settings are limited; depth maps are generated with mist pass instead
    layer.use_pass_z = False


def compositor_setup():
    """
    Currently unused.
    Creates node setup for exporting .exr files (currently done with render.render()).

    PROS:   Easy to add new layer to the .exr if needed
            Should be used if also using compositor preprocessing (denoising, 2D image effects ...)
    CONS:   "File Output" node adds current frame number to filename; there is no setting to turn this off
    """

    scene = bpy.context.scene
    compositor_tree = scene.node_tree

    render_setup()

    scene.use_nodes = True
    scene.render.use_compositing = True

    for node in compositor_tree.nodes:
        compositor_tree.nodes.remove(node)

    node_render_layers = compositor_tree.nodes.new(type='CompositorNodeRLayers')
    node_composite = compositor_tree.nodes.new(type='CompositorNodeComposite')
    node_output_file = compositor_tree.nodes.new(type='CompositorNodeOutputFile')

    node_output_file.file_slots.clear()
    node_output_file.file_slots.new(name='Color')
    node_output_file.file_slots.new(name='Depth')
    node_output_file.file_slots.new(name='Normal')

    node_output_file.format.file_format = 'OPEN_EXR_MULTILAYER'
    node_output_file.format.color_management = 'FOLLOW_SCENE'
    node_output_file.base_path = bpy.context.scene.render.filepath
    node_output_file.format.color_depth = '16'

    node_render_layers.location = (0, 0)
    node_composite.location = (500, 0)
    node_output_file.location = (500, -200)

    compositor_tree.links.new(node_render_layers.outputs['Image'], node_composite.inputs['Image'])
    compositor_tree.links.new(node_render_layers.outputs['Image'], node_output_file.inputs['Color'])
    compositor_tree.links.new(node_render_layers.outputs['Mist'], node_output_file.inputs['Depth'])
    compositor_tree.links.new(node_render_layers.outputs['Normal'], node_output_file.inputs['Normal'])
