import bpy

C = bpy.context
D = bpy.data

# ----------------------------------------
script_dir = "C:\\Users\\PetoK\\Downloads"

blendfile = script_dir + "\\92-plastic.blend"
selection = "\\Material\\"
material_name = "plastic"

filepath = blendfile + selection + material_name
directory = blendfile + selection
filename = material_name
# ----------------------------------------

def link_material_lib(filepath, directory, filename):
    # todo linux paths & use "with" & split filename
    bpy.ops.wm.link(filepath=filepath, directory=directory, filename=filename)

def get_linked_materials():
    return list(filter(lambda mat: mat.library is not None, D.materials.values()))

def get_linked_materials(lib):
    return list(filter(lambda mat: mat.library == lib, D.materials.values()))



C.active_object.active_material = material