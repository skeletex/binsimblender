import bpy
import os

"""
This script creates material preview file for BinSimBlender.
It gathers all materials from a given directory and copies
them into currently open blend file (run from text editor).
"""

C = bpy.context
D = bpy.data

def delete_objects():
    for obj in D.objects:
        D.objects.remove(obj)

def delete_materials():
    for mat in D.materials:
        D.materials.remove(mat)

def link_material_lib(filepath):
    with bpy.data.libraries.load(filepath) as (data_from, data_to):
        data_to.materials = data_from.materials

def set_fake_users():
    mats = bpy.data.materials
    mats.foreach_set("use_fake_user", [True] * len(mats))

def order_in_grid(objects, line_len):
    obj_location = (0, 0, 0)
    for obj in D.objects:
        obj.location = obj_location
        
        obj_location = obj_location[0] + 3, obj_location[1], obj_location[2]
        if obj_location[0] >= line_len * 3:
            obj_location = 0, obj_location[1] + 3, obj_location[2]

def create_preview(line_len):
    for material in D.materials:
        bpy.ops.mesh.primitive_uv_sphere_add()
        C.active_object.active_material = material
        bpy.ops.object.shade_smooth()
    order_in_grid(D.objects, line_len)
    


delete_objects()
delete_materials()

blend_files = []
walked = os.walk("C:\\Skeletex\\GIT\\shared\\codebase\\cmake-build-release\\applications\\BinGenerator\\data\\materials")

for dirpath, dirnames, filenames in walked:
    blend_files_here = list(filter(lambda filename : filename.endswith('.blend'), filenames))
    for filename in blend_files_here:
        blend_path = os.path.join(dirpath, filename)
        blend_files.append(blend_path)

for path in blend_files:
    link_material_lib(path)
set_fake_users()
create_preview(8)
