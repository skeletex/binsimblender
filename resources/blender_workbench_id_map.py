import bpy

"""
I've experimented with ways of extracting ID map out of blender.
The options include:
    ID render pass - practically free but limited to cycles
    workbench id
    any engine random color - free but can't easily match part to id
    eevee AOV
    eevee cryptomatte
    eevee custom shader with color = object ID

downside of rendering in different engine: render time goes up
because combined pass renders every time and can't be disabled
"""

for id, obj in enumerate(bpy.data.objects):
    obj.pass_index = id
    obj.color = (id * 0.01, id * 0.01, id * 0.01, 1.0)
    
    
scene = bpy.context.scene

scene.render.engine = 'BLENDER_WORKBENCH'
scene.shading.light = 'FLAT'
scene.shading.color_type = 'ATTRIBUTE'

bpy.data.scenes["Scene"].shading.light
