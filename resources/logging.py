import bpy
import os

"""
This was an unsuccessful attempt at making Blender logs more verbose.
Doesn't work in headless mode!

Some actions within Blender don't appear in Blender logs (stdout) even when verbose
e.g. property changes. They do appear in Blender's info panel and can be copied.
Blender also exposes history of all operator calls from window manager.

The following functions only work on changes executed manually. It appears
that operators and property changes made by API calls
are not logged anywhere by Blender.

Alternative way of logging: bpy.msgbus & bpy.app.handlers.
Limited to RNA changes (logs property changes, doesn't log operators).
Handler notifications are severely limited. They don't provide information
about the caller or what exactly changed.
"""


def log_info(filepath="./blender_log_info.txt"):
    C = bpy.context

    with open(os.path.abspath(filepath), 'w') as log:
        # bpy.context.area is None by default in headless mode
        with C.temp_override(area=C.screen.areas[0]):
            bpy.context.area.type = 'INFO'

            # swap area, call op, swap area back
            bpy.ops.info.select_all(action='SELECT')
            bpy.ops.info.report_copy()

            print(bpy.context.window_manager.clipboard, file=log)


def log_operator_history(filepath="./blender_op_history.txt"):
    C = bpy.context

    with open(os.path.abspath(filepath), 'w') as log:
        print("\nOperator count: ", len(list(C.window_manager.operators)), file=log)
        for id, operator in enumerate(C.window_manager.operators):
            props = C.window_manager.operator_properties_last(operator.bl_idname)

            print(id, operator.bl_idname, operator.name, file=log)
            for key, value in list(props.items()):
                to_list = getattr(value, "to_list", None)
                if to_list is not None:
                    print("\t", key, ":", to_list(), file=log)
                else:
                    print("\t", key, ":", value, file=log)


def log_all():
    log_info()
    log_operator_history()
