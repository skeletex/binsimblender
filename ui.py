import bpy
from bpy.types import Panel


class BINSIM_PT_settings(bpy.types.Panel):
	bl_idname = "BINSIM_PT_settings"
	bl_label = "BinSim Settings"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	bl_category = "BinSim"

	def draw(self, context):
		layout = self.layout
		layout.use_property_split = True
		layout.use_property_decorate = False

		scene = bpy.context.scene
		binsim_data = scene.bs_data

		layout.prop_search(binsim_data, "bin_template", scene, "objects")
		layout.prop_search(binsim_data, "part_template", scene, "objects")
		layout.separator()
		layout.prop(binsim_data, "pattern_dir")
		layout.prop(binsim_data, "pattern_image_smooth")
		layout.prop(binsim_data, "pattern_include_none")
		

class BINSIM_PT_preview(bpy.types.Panel):
	
	#TODO Why is this panel not working????
	
	bl_idname = "BINSIM_PT_preview"
	bl_label = "Scene Preview"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	bl_category = "BinSim"

	def draw(self, context):
		layout = self.layout
		layout.use_property_split = True
		layout.use_property_decorate = False

		binsim_data = bpy.context.scene.bs_data

		layout.prop(binsim_data, "scene_map_file")
		layout.prop(binsim_data, "proj_energy")
		layout.operator("binsim.preview", icon="FILE_REFRESH", text="Preview Scene")
		

def register():
	bpy.utils.register_class(BINSIM_PT_settings)
	bpy.utils.register_class(BINSIM_PT_preview)


def unregister():
	bpy.utils.unregister_class(BINSIM_PT_settings)
	bpy.utils.unregister_class(BINSIM_PT_preview)
